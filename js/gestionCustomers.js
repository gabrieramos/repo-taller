var clientesObtenidos;
function getCustomers() {
  //var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function () { // configuración de la petición basada en eventos
  if(this.readyState == 4 && this.status == 200) // con 4 se verifica que ya cargó la consulta y con 200 que fue de manera correcta
  {
      //console.log(request.responseText);
       clientesObtenidos=request.responseText;
       procesaClientes();
  }
 }
 request.open("GET", url, true); // se está enviando una petición de tipo get; true indica que es un petición de tipo síncrona
 request.send();
}

function procesaClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos); // con el metodo PARSE se está convierto a unobjetovo de javascript
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody=document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i< JSONClientes.value.length; i++)
  {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");

    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    var bandera =  JSONClientes.value[i].Country;
    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
    var imgBandera=document.createElement("img");
    imgBandera.classList.add("flag");

    if(bandera == "UK"){
      bandera = "United-Kingdom";
    }
    imgBandera.src=rutaBandera + bandera + ".png";
    var columnaBandera = document.createElement("td");
    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);

  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
