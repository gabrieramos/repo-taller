var productosObtenidos;
function getProductos() {
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
    request.onreadystatechange = function () { // configuración de la petición basada en eventos
  if(this.readyState == 4 && this.status == 200) // con 4 se verifica que ya cargó la consulta y con 200 que fue de manera correcta
  {
      //console.log(request.responseText);
      productosObtenidos= request.responseText;
      procesarProductos();
  }
 }
 request.open("GET", url, true); // se está enviando una petición de tipo get; true indica que es un petición de tipo síncrona
 request.send();
}

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos); // con el metodo PARSE se está convierto a unobjetovo de javascript
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody=document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i< JSONProductos.value.length; i++)
  {
    //console.console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);

  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
